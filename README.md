Semestrální projekt simulace **Smart Home**

**Abstrakt**
Vytvořit aplikaci pro virtuální simulaci inteligentního domu, kde simulujeme chod domácnosti, používáme jednotlivá zařízení domu a vyhodnocujeme využití, spotřebu, volný a pracovní čas jednotlivých osob. \


Základní chod programu: Main -> ConfigurationBuilder + run Simulation \
ConfigurationBuilder, tvorba a nastavení objektů \
Simulaion, hlavní cyklus programu, běží podle nastavení proměnné **phases** (defaultně = 100)

**Funkční požadavky**

**F1:** Splněno, jsou použity entity jako dům, patro v domu, místnost a různé druhy zařízení/spotřebičů, osob a zvířat.

**F2:** Splněno, každé zařízení se může přepínat mezi stavy ON, OFF a IDLE + Lednice má obsah a při jeho dostatečném úbytku je požádán nějaký člen rodiny o procházku s cílem nakoupit. Dále se každé zařízení může rozbít. Některé se povede opravit, některé však potřebují reklamaci nebo rovnou vyhodit.

**F3:** Splněno, v každém stavu má dané zařízení různou spotřebu.

**F4:** Splněno, většina zařízení spotřebovává pouze elektřinu, například ale u pračky se sbírají data i o vodě. U plynové kotle pro změnu zase informace o spotřebě plynu. Všechna zařízení mají svoji hodnotu funkčnosti, která když klesne na nulu, tak se zařízení rozbije a některý z obyvatel domu se ho následně pokusí opravit. V případě neúspěšné opravy je potřeba zkusit zařízení protlačit na reklemaci nebo vyhodit.

**F5:** Splněno, jsou celkem tři druhy akcí: \
    1. Daný objekt zvládne uskutečnit sám o sobě, např. když osoba dojde energie, jde si na chvíli odpočinout. \
    2. Daný objekt potřebuje tuto akci poslat na handler simulace, např. když si osoba vezme jídlo z ledničky nebo chce přejít do jiné místnosti. \
    3. Daný objekt potřebuje tuto akci poslat na handler simulace + potřebuje nějakého vykonavatele této akce, např. oprava zařízení, jít na nákup, přebalit dítě, vyvenčit zvíře, atd.

**F6:** Částečně splněno, zařízení, osoby i zvířata se nacházejí v každém okamžiku právě v jedné místnosti.

**F7:** Splněno, viz. předchozí body.

**F8:** Splněno, nakonec za použití tří reportů: \
    1. ConfigurationReport, který popisuje hieararchicky veškerou konfiguraci domu. \
    2. ActivityEventReport, který zaznameváná veškeré aktivity, eventy a použití zařízení za celý chod simulace. \
    3. ConsumptionReport, který ukazuje veškerou spotřebu energií a opotřebení zařízení v domu.

**F9:** Nesplněno

**F10:** Nesplněno

**Použité design patterny**
- Observer
- Visitor
- State
- Builder

**Požadované výstupy**
- Class diagram je v /SmartHome/classModel.png, Use case diagram chybí
- Javadoc je v /SmartHome/doc
- Konfiguraci mám jenom jednu ve formě třídy přímo v programu, v balíčku configuration
