package andrajak.smarthome.configuration;

import andrajak.smarthome.Handler;
import andrajak.smarthome.event.Event;
import andrajak.smarthome.creature.animal.Animal;
import andrajak.smarthome.creature.person.Adult;
import andrajak.smarthome.creature.person.Person;
import andrajak.smarthome.place.House;
import andrajak.smarthome.report.ConfigurationReport;
import andrajak.smarthome.report.ConsumptionReport;
import andrajak.smarthome.report.WritableActivityEventReport;
import andrajak.smarthome.report.ActivityEventReport;
import andrajak.smarthome.device.Device;

import java.util.List;
import java.util.Random;

public class Simulation implements WritableActivityEventReport {

    private static final int phases = 100;
    private static final Random random = new Random();
    private static Handler handler;
    private final House house;
    private final ActivityEventReport activityEventReport;
    private final ConfigurationReport configurationReport;
    private final ConsumptionReport consumptionReport;


    public Simulation(House house) {
        this.house = house;
        this.activityEventReport = new ActivityEventReport();
        this.configurationReport = new ConfigurationReport();
        this.consumptionReport = new ConsumptionReport();
        handler = new Handler(house);
    }

    public void run() {
        configurationReport.generateReport(house);

        for (int phase = 1; phase <= phases; phase++) {
            write("");
            write("Fáze č. " + phase);
            write("-----------------------------");

            List<Person> allPersons = house.getAllPersons();
            for (int i = 0; i < allPersons.size(); i++) {
                Person randomPerson = getRandomPerson(allPersons);
                if (randomPerson != null){
                    randomPerson.tick();
                }
            }

            List<Animal> allAnimals = house.getAllAnimals();
            for (int i = 0; i < allAnimals.size(); i++) {
                Animal randomAnimal = getRandomAnimal(allAnimals);
                if (randomAnimal != null){
                    randomAnimal.tick();
                }
            }

            List<Device> allDevices = house.getAllDevices();
            for (int i = 0; i < allDevices.size(); i++) {
                Device randomDevice = getRandomDevice(allDevices);
                if (randomDevice != null){
                    randomDevice.tick();
                }
            }

            int stopResolving = random.nextInt(10);
            while (!house.getAllEvents().isEmpty() && stopResolving != 1 ) {
                Event firstEvent = house.getAllEvents().remove(0);
                if (!firstEvent.isNeedSolver())
                    handler.resolveEvent(firstEvent);
                else {
                    Person randomPerson = getRandomPerson(house.getAllPersons());
                    if (randomPerson != null)
                        handler.resolveEvent(firstEvent, randomPerson);
                }
            }
        }

        activityEventReport.endReport();
        consumptionReport.generateReport(house);
    }

    private Device getRandomDevice(List<Device> allDevices) {
        if (allDevices.size() != 0){
            return allDevices.get(random.nextInt(allDevices.size()));
        }
        return null;
    }

    private Animal getRandomAnimal(List<Animal> allAnimals) {
        if (allAnimals.size() != 0){
            return allAnimals.get(random.nextInt(allAnimals.size()));
        }
        return null;
    }

    private Person getRandomPerson(List<Person> allPersons) {
        if (allPersons.size() != 0){
            return allPersons.get(random.nextInt(allPersons.size()));
        }
        return null;
    }

    private Person getRandomAdultFemale(List<Person> allPersons) {
        if (allPersons.size() != 0){
            for (int i = 0; i < allPersons.size(); i++) {
                Person person = allPersons.get(random.nextInt(allPersons.size()));
                if (person instanceof Adult && person.getSex() == Person.Sex.female)
                    return person;
            }
        }
        return null;
    }

}
