package andrajak.smarthome.configuration;

import andrajak.smarthome.creature.animal.Animal;
import andrajak.smarthome.creature.animal.Cat;
import andrajak.smarthome.creature.animal.Dog;
import andrajak.smarthome.creature.person.Adult;
import andrajak.smarthome.creature.person.Baby;
import andrajak.smarthome.creature.person.Child;
import andrajak.smarthome.creature.person.Person;
import andrajak.smarthome.device.*;
import andrajak.smarthome.place.House;
import andrajak.smarthome.place.HouseBuilder;

import java.util.ArrayList;
import java.util.List;

public class ConfigurationBuilder {

    static public House build() {

        List<Person> livingroomPersons = new ArrayList<>();
        livingroomPersons.add(new Adult("Dušan", Person.Sex.male));
        livingroomPersons.add(new Child("Jakub", Person.Sex.male));
        List<Animal> livingroomAnimals = new ArrayList<>();
        livingroomAnimals.add(new Dog("Max"));
        List<Device> livingroomDevices = new ArrayList<>();
        livingroomDevices.add(new AirConditioning("klimatizace A"));
        livingroomDevices.add(new Television("televize"));

        List<Person> kitchenPersons = new ArrayList<>();
        kitchenPersons.add(new Adult("Dana", Person.Sex.female));
        kitchenPersons.add(new Adult("Bára", Person.Sex.female));
        List<Animal> kitchenAnimals = new ArrayList<>();
        List<Device> kitchenDevices = new ArrayList<>();
        kitchenDevices.add(new Fridge("lednička"));
        kitchenDevices.add(new GasBoiler("plynový kotel"));
        kitchenDevices.add(new Microwave("mikrovlnka"));
        kitchenDevices.add(new Toaster("toustovač"));

        List<Person> stuffroomPersons = new ArrayList<>();
        List<Animal> stuffroomAnimals = new ArrayList<>();
        List<Device> stuffroomDevices = new ArrayList<>();
        stuffroomDevices.add(new WashingMachine("pračka"));
        stuffroomDevices.add(new Hoover("vysavač"));

        List<Person> bathroomPersons = new ArrayList<>();
        bathroomPersons.add(new Child("Dušana", Person.Sex.female));
        List<Animal> bathroomAnimals = new ArrayList<>();
        List<Device> bathroomDevices = new ArrayList<>();

        List<Person> bedroomPersons = new ArrayList<>();
        List<Animal> bedroomAnimals = new ArrayList<>();
        List<Device> bedroomDevices = new ArrayList<>();
        bedroomDevices.add(new AirConditioning("klimatizace B"));

        List<Person> childroomPersons = new ArrayList<>();
        childroomPersons.add(new Baby("Ďábel malý", Person.Sex.male));
        List<Animal> childroomAnimals = new ArrayList<>();
        childroomAnimals.add(new Cat("Bublina"));
        List<Device> childroomDevices = new ArrayList<>();

        HouseBuilder builder = new HouseBuilder();
        House house = builder.
                setHouseName("Domov 2.0").
                addFloor(0).
                addRoom(0, "kuchyň", kitchenPersons, kitchenAnimals, kitchenDevices).
                addRoom(0, "koupelna", bathroomPersons, bathroomAnimals, bathroomDevices).
                addRoom(0, "obývací pokoj", livingroomPersons, livingroomAnimals, livingroomDevices).
                addFloor(1).
                addRoom(1, "ložnice", bedroomPersons, bedroomAnimals, bedroomDevices).
                addRoom(1, "dětský pokoj", childroomPersons, childroomAnimals, childroomDevices).
                addRoom(1,"kumbál", stuffroomPersons, stuffroomAnimals, stuffroomDevices).
                getResult();

        for (Person person : house.getAllPersons())
            person.attach(house);

        for (Animal animal : house.getAllAnimals())
            animal.attach(house);

        for (Device device : house.getAllDevices())
            device.attach(house);

        return house;
    }
}
