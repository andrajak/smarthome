package andrajak.smarthome.observer;

import andrajak.smarthome.event.Event;

public interface Observer {

    void update(Event event);
}