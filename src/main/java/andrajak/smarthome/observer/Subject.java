package andrajak.smarthome.observer;

import andrajak.smarthome.event.Event;

public interface Subject {

    void attach(Observer observer);
    void detach(Observer observer);
    void notifyAllObservers(Event event);
}