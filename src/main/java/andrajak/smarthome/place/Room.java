package andrajak.smarthome.place;

import java.util.ArrayList;
import java.util.List;

import andrajak.smarthome.creature.animal.Animal;
import andrajak.smarthome.creature.person.Person;
import andrajak.smarthome.device.Device;

public class Room {

    private String name;
    private List<Person> persons = new ArrayList<>();
    private List<Animal> animals = new ArrayList<>();
    private List<Device> devices = new ArrayList<>();

    public Room(String name) {
        this.name = name;
    }

    public Room(String name, List<Person> persons, List<Animal> animals, List<Device> devices) {
        this.name = name;
        this.persons = persons;
        this.animals = animals;
        this.devices = devices;

        //if (persons != null)
        for (Person person : this.persons)
            person.setRoom(this);
    }

    public void addPerson(Person person) {
        persons.add(person);
    }

    public void addAnimal(Animal animal) {
        animals.add(animal);
    }

    public void addDevice(Device device) {
        devices.add(device);
    }

    public void deleteDevice(Device device) {
        devices.remove(device);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Device> getDevices() { return devices; }

    public void setDevices(List<Device> devices) { this.devices = devices; }

    public List<Person> getPersons() { return persons; }

    public void setPersons(List<Person> persons) { this.persons = persons; }

    public List<Animal> getAnimals() { return animals; }

    public void setAnimals(List<Animal> animals) { this.animals = animals; }
}
