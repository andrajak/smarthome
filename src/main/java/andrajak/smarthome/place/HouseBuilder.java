package andrajak.smarthome.place;

import andrajak.smarthome.creature.animal.Animal;
import andrajak.smarthome.creature.person.Person;
import andrajak.smarthome.device.Device;

import java.util.List;

public class HouseBuilder {

    private final House house = new House();;

    public HouseBuilder() {}

    public HouseBuilder setHouseName(String name) {
        house.setName(name);
        return this;
    }

    public HouseBuilder addFloor(int high) {
        this.house.addFloor(high);
        return this;
    }

    public HouseBuilder addRoom(int high, String name) {
        this.house.addRoom(high, name);
        return this;
    }

    public HouseBuilder addRoom(int high, String name, List<Person> persons, List<Animal> animals, List<Device> devices) {
        this.house.addRoom(high, name, persons, animals, devices);
        return this;
    }

    public House getResult() {
        return this.house;
    }
}
