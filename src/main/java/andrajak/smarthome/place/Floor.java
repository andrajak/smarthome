package andrajak.smarthome.place;

import java.util.ArrayList;
import java.util.List;

import andrajak.smarthome.creature.animal.Animal;
import andrajak.smarthome.creature.person.Person;
import andrajak.smarthome.device.Device;

public class Floor {

    private int high;
    private List<Room> rooms = new ArrayList<>();

    public Floor(int high) {
        this.high = high;
    }

    public Floor(int high, List<Room> rooms) {
        this.high = high;
        this.rooms = rooms;
    }

    public void addRoom(String name) {
        this.rooms.add(new Room(name));
    }

    public void addRoom(String name, List<Person> persons, List<Animal> animals, List<Device> devices) {
        this.rooms.add(new Room(name, persons, animals, devices));
    }

    public int getHigh() {
        return high;
    }

    public void setHigh(int high) {
        this.high = high;
    }

    public List<Room> getRooms() {
        return rooms;
    }

    public void setRooms(List<Room> rooms) {
        this.rooms = rooms;
    }
}
