package andrajak.smarthome.place;

import java.util.ArrayList;
import java.util.List;

import andrajak.smarthome.creature.animal.Animal;
import andrajak.smarthome.creature.person.Person;
import andrajak.smarthome.device.Device;
import andrajak.smarthome.event.Event;
import andrajak.smarthome.observer.Observer;

public class House implements Observer {

    private String name;
    private List<Floor> floors = new ArrayList<>();
    private List<Event> events = new ArrayList<>();

    public House() {}

    public House(String name) {
        this.name = name;
    }

    public void addFloor(int high) {
        this.floors.add(new Floor(high));
    }

    public void addRoom(int high, String name) {
        Floor floor = getFloorByHigh(high);
        if (floor != null) {
            floor.addRoom(name);
        }
    }

    public void addRoom(int high, String name, List<Person> persons, List<Animal> animals, List<Device> devices) {
        Floor floor = getFloorByHigh(high);
        if (floor != null) {
            floor.addRoom(name, persons, animals, devices);
        }
    }

    private Floor getFloorByHigh(int high) {
        for (Floor floor : this.floors) {
            if (floor.getHigh() == high)
                return floor;
        }
        return null;
    }

    public void deleteDevice(Device specificDevice) {
        for(Floor floor : this.floors) {
            for (Room room : floor.getRooms()) {
                for (Device device : room.getDevices()) {
                    if (specificDevice.getName().equals(device.getName())) {
                        room.deleteDevice(specificDevice);
                        return;
                    }
                }
            }
        }
    }

    public List<Person> getAllPersons() {
        List<Person> allPersons = new ArrayList<>();
        for(Floor floor : this.floors) {
            for (Room room : floor.getRooms()) {
                allPersons.addAll(room.getPersons());
            }
        }
        return allPersons;
    }

    public List<Animal> getAllAnimals() {
        List<Animal> allAnimals = new ArrayList<>();
        for(Floor floor : this.floors) {
            for (Room room : floor.getRooms()) {
                allAnimals.addAll(room.getAnimals());
            }
        }
        return allAnimals;
    }

    public List<Device> getAllDevices() {
        List<Device> allDevices = new ArrayList<>();
        for(Floor floor : this.floors) {
            for (Room room : floor.getRooms()) {
                allDevices.addAll(room.getDevices());
            }
        }
        return allDevices;
    }

    @Override
    public void update(Event event) {
        events.add(event);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Floor> getFloors() {
        return floors;
    }

    public void setFloors(List<Floor> floors) {
        this.floors = floors;
    }

    public List<Event> getAllEvents() {
        return events;
    }

    public void setAllEvents(List<Event> allEvents) { this.events = allEvents; }
}
