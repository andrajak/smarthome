package andrajak.smarthome;

import andrajak.smarthome.creature.person.Adult;
import andrajak.smarthome.device.Device;
import andrajak.smarthome.device.Fridge;
import andrajak.smarthome.event.*;
import andrajak.smarthome.creature.person.Person;
import andrajak.smarthome.place.House;
import andrajak.smarthome.report.WritableActivityEventReport;

import java.util.Random;

import static andrajak.smarthome.device.Constants.FRIDGE_CAPACITY;
import static andrajak.smarthome.device.Constants.LIFETIME;

public class Handler implements WritableActivityEventReport {

    private final House house;

    public Handler(House house) {
        this.house = house;
    }

    public  void resolveEvent(Event event) {
        switch (event.getType()) {
            case TAKE_FOOD:
                if (probabilityOneToX(2)) {
                    Fridge fridge = getRandomFridge();
                    if (fridge != null) {
                        write(((TakeFood) event).getPerson().getName() + " má hlad, bere si jídlo z lednice.");
                        fridge.minusFood();
                    }
                }
                break;
            case CHANGE_ROOM:

        }
    }

    public void resolveEvent(Event event, Person person)  {
        switch (event.getType()){
            case BREAK:
                if (person.getType() == Person.Type.adult && person.getSex() == Person.Sex.male) {
                    write(person.getName() + " opravil " + ((Break) event).getDevice().getName());
                    ((Break) event).getDevice().setLifetime(LIFETIME);
                }
                else if (probabilityOneToX(2)) {
                    write(person.getName() + " se hecnul a opravil " + ((Break) event).getDevice().getName());
                    ((Break) event).getDevice().setLifetime(LIFETIME);
                }
                else {
                    write(((Break)event).getDevice().getName() + " je težcě rozbitý, tohle už nepůjde. Musíme to vyhodit.");
                    ((Break) event).getDevice().setAlive(false);
                }
                break;
            case FILL_FRIDGE:
                if (probabilityOneToX(1) && person instanceof Adult) {
                    write(person.getName() + " došel na nákup.");
                    ((Fridge)((FillFridge)event).getDevice()).setCapacity(FRIDGE_CAPACITY);
                }
                else
                    write(person.getName() + " by šel na nákup rád, ale nemůže.");
                break;
            case PEE:
                if (probabilityOneToX(4))
                    write(person.getName() + " vyvenčil " + ((Pee)event).getAnimal().getName());
                else
                    write(((Pee)event).getAnimal().getName() + " se brzo počůrá. Smutně, smutně kouká.");
                break;
            case REPACK:
                if (probabilityOneToX(3) && person.getSex() == Person.Sex.female)
                    write(person.getName() + " přebalil " + ((Repack)event).getPerson().getName());
                else
                    write(((Repack)event).getPerson().getName() + " potřebuje nutně přebalit nebo brzo dojde ke katastrofě.");
                break;
            case HUNGRY:
                if (probabilityOneToX(3))
                    write(person.getName() + " nakrmil " + ((Hungry)event).getAnimal().getName());
                else
                    write(((Hungry)event).getAnimal().getName() + " kručí v bříšku.");
                break;
        }
    }

    private boolean probabilityOneToX(int x) {
        return new Random().nextInt(x) == 0;
    }

    private Fridge getRandomFridge() {
        for (Device device : house.getAllDevices()) {
            if (device instanceof Fridge)
                return (Fridge) device;
        }
        return null;
    }
}
