package andrajak.smarthome.report;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import andrajak.smarthome.place.House;
import andrajak.smarthome.device.Device;

import static andrajak.smarthome.device.Constants.*;

public class ConsumptionReport {

    public static FileOutputStream report;

    public ConsumptionReport() {}

    public void generateReport(House house) {
        try {
            report = new FileOutputStream("reports/ConsumptionReport.txt");
        } catch (FileNotFoundException e) {
            System.out.println("WAR: Report nelze vytvořit nebo otevřít!\n" + e);
        }

        write("///------------------ ZAČÁTEK REPORTU ------------------///");
        write(" ");

        if (house != null)
            printHouseToReport(house);
        else
            write("..Hups, dům neexistuje nebo došlo v nečekaných komplikacím..");
    }

    private double calculatePrice(double electricityValue, double waterValue, double gasValue) {
        return gasValue*GAS_PRICE + waterValue*WATER_PRICE + electricityValue*ELECTRICITY_PRICE;
    }

    private void write(String line) {
        line += System.lineSeparator();
        try {
            report.write(line.getBytes());
        } catch (IOException e) {
            Logger.getLogger(ConsumptionReport.class.getName()).log(Level.WARNING, "WAR: Řádka nebyla úspěšně zapsána do reportu!", e);
        }
    }

    private void printHouseToReport(House house) {
        double gas = 0, water = 0, electricity = 0;
        for (Device device : house.getAllDevices()) {
            gas += device.getTotalGas();
            water += device.getTotalWater();
            electricity += device.getTotalElectricity();
        }
        double totalPrice = calculatePrice(electricity, water, gas);
        write("Energie domu " + house.getName());
        write("---");
        write("Plyn:        " + gas);
        write("Voda:        " + water);
        write("Elektřina:   " + electricity);
        write("Celkové výdaje domu:   " + totalPrice + " Kč");
        write("------------------------------------");
        write(" ");

        for (Device device : house.getAllDevices()) {
            printDeviceToReport(device);
        }

        write("///------------------- KONEC REPORTU -------------------///");
        try {
            report.close();
        } catch (IOException e) {
            System.out.println("WAR: Report nelze korektně ukončit!" + e);
        }
    }

    private void printDeviceToReport(Device device) {
        double gas = device.getTotalGas();
        double water = device.getTotalWater();
        double electricity = device.getTotalElectricity();
        double totalPrice = calculatePrice(electricity, water, gas);
        if (device.isAlive()) {
            write("   Spotřeba zařízení " + device.getName());
            write("   Aktuální funkčnost je: " + device.getLifetime() + "%");
        }
        else {
            write("   " + device.getName() + " potřebuje reklamaci nebo vyhodit!");
            write("   Jeho spotřeba po dobu provozu byla:");
        }
        write("   ---");
        write("   Plyn:        " + gas);
        write("   Voda:        " + water);
        write("   Elektřina:   " + electricity);
        write("   Celkové výdaje zařízení:   " + totalPrice + " Kč");
        write("   ------------------------------------");
        write("   ");
    }

}
