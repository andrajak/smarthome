package andrajak.smarthome.report;

import java.io.*;

public class ActivityEventReport implements WritableActivityEventReport {

    public static FileOutputStream report;

    public ActivityEventReport() {
        try {
            report = new FileOutputStream("reports/ActivityEventReport.txt");
            write("///------------------ ZAČÁTEK REPORTU ------------------///");
        } catch (FileNotFoundException e) {
            System.out.println("WAR: Report nelze vytvořit nebo otevřít!\n" + e);
        }
    }

    public void endReport(){
        write("");
        write("///------------------- KONEC REPORTU -------------------///");
        try {
            report.close();
        } catch (IOException e) {
            System.out.println("WAR: Report nelze korektně ukončit!" + e);
        }
    }
}
