package andrajak.smarthome.report;

import java.io.IOException;

import static andrajak.smarthome.report.ConfigurationReport.report;

public interface WritableConfigurationReport {
    default void write(String line) {
        line += System.lineSeparator();
        try {
            report.write(line.getBytes());
        } catch (IOException e) {
            System.out.println("WAR: Řádka nebyla úspěšně zapsána do reportu!\n" + e);
        }
    }
}
