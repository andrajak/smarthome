package andrajak.smarthome.report;

import andrajak.smarthome.creature.animal.Animal;
import andrajak.smarthome.creature.person.Person;
import andrajak.smarthome.device.Device;
import andrajak.smarthome.place.Floor;
import andrajak.smarthome.place.House;
import andrajak.smarthome.place.Room;
import andrajak.smarthome.visitor.Visitor;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ConfigurationReport implements Visitor, WritableConfigurationReport {

    public static FileOutputStream report;

    public ConfigurationReport() {}

    public void generateReport(House house){
        try {
            report = new FileOutputStream("reports/ConfigurationReport.txt");
        } catch (FileNotFoundException e) {
            System.out.println("WAR: Report nelze vytvořit nebo otevřít!\n" + e);
        }

        write("///------------------ ZAČÁTEK REPORTU ------------------///");
        write(" ");

        if (house != null)
            visit(house);
        else
            write("..Hups, dům neexistuje nebo došlo v nečekaných komplikacím..");
    }

    @Override
    public void visit(House house) {
        write(house.getName());
        write("----------------");
        write("");
        int floors = house.getFloors().size();
        for (int i = 0; i < floors; i++){
            visit(house.getFloors().get(i));
        }

        write("");

        List<Person> persons = house.getAllPersons();
        List<Person> adults = new ArrayList<>();
        List<Person> children = new ArrayList<>();
        List<Person> babies = new ArrayList<>();
        for (Person person : persons) {
            if (person.getType() == Person.Type.adult) {
                adults.add(person);
                continue;
            }
            if (person.getType() == Person.Type.child) {
                children.add(person);
                continue;
            }
            if (person.getType() == Person.Type.baby) {
                babies.add(person);
                continue;
            }
        }
        write("  Obyvatel domu celkem: " + persons.size());
        write("     Dospělý:");
        for (Person person : adults)
            visit(person);
        write("     Děti:");
        for (Person person : children)
            visit(person);
        write("     Mimina:");
        for (Person person : babies)
            visit(person);

        write("\n");

        List<Animal> animals = house.getAllAnimals();
        write("  Zvířat v domě celkem: " + animals.size());
        for (Animal animal : animals) {
            visit(animal);
        }

        write("");
        write("///------------------- KONEC REPORTU -------------------///");
        try {
            report.close();
        } catch (IOException e) {
            System.out.println("WAR: Report nelze korektně ukončit!" + e);
        }
    }

    @Override
    public void visit(Floor floor) {
        switch (floor.getHigh()) {
            case 0 -> write("   Přízemí");
            case 1 -> write("   První patro");
            case 2 -> write("   Druhé patro");
            default -> write("   " + floor.getHigh() + ". patro");
        }
        int rooms = floor.getRooms().size();
        for (int i = 0; i < rooms; i++){
            visit(floor.getRooms().get(i));
        }
        write("   ---------------------------");
        write("");
    }

    @Override
    public void visit(Room room) {
        write("      " + room.getName());
        int devices = room.getDevices().size();
        for (int i = 0; i < devices; i++){
            visit(room.getDevices().get(i));
        }
    }

    @Override
    public void visit(Person person) {
        write("       " + person.getName());
    }

    @Override
    public void visit(Animal animal) {
        write("     " + animal.getType() + " jménem " + animal.getName());
    }

    @Override
    public void visit(Device device) {
        write("        " + device.getName());
    }
}
