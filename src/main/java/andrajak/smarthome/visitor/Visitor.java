package andrajak.smarthome.visitor;

import andrajak.smarthome.creature.animal.Animal;
import andrajak.smarthome.creature.person.Person;
import andrajak.smarthome.place.House;
import andrajak.smarthome.place.Floor;
import andrajak.smarthome.place.Room;
import andrajak.smarthome.device.Device;

public interface Visitor {
    void visit(House house);
    void visit(Floor floor);
    void visit(Room room);
    void visit(Person person);
    void visit(Animal animal);
    void visit(Device device);
}
