package andrajak.smarthome;

import andrajak.smarthome.configuration.ConfigurationBuilder;
import andrajak.smarthome.configuration.Simulation;
import andrajak.smarthome.place.House;

public class Main {

    static House house;

    public static void main(String[] args) {

        house = ConfigurationBuilder.build();
        Simulation simulation = new Simulation(house);
        simulation.run();
    }

}
