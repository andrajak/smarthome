package andrajak.smarthome.device;

import andrajak.smarthome.report.WritableActivityEventReport;

import static andrajak.smarthome.device.Constants.AIR_CONDITIONING;
import static andrajak.smarthome.device.Constants.GAS_BOILER;

public class GasBoiler extends Device implements WritableActivityEventReport {

    public GasBoiler(String name) {
        super(name);
    }

    @Override
    public void calculatedForState(double stateValue) {
        setTotalGas(getTotalGas() + stateValue * GAS_BOILER);
    }
}
