package andrajak.smarthome.device;

import andrajak.smarthome.report.WritableActivityEventReport;

import static andrajak.smarthome.device.Constants.*;

public class WashingMachine extends Device implements WritableActivityEventReport {

    public WashingMachine(String name) {
        super(name);
    }

    @Override
    public void calculatedForState(double stateValue) {
        setTotalElectricity(getTotalElectricity() + stateValue * WASHING_MACHINE_ELECTRICITY);
        setTotalWater(getTotalWater() + stateValue * WASHING_MACHINE_WATER);
    }

}
