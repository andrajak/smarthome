package andrajak.smarthome.device.state;

import andrajak.smarthome.device.Device;
import andrajak.smarthome.report.WritableActivityEventReport;

public class IdleState implements DeviceState, WritableActivityEventReport {

    @Override
    public void turnIdle(Device device) {
    }

    @Override
    public void turnOff(Device device) {
        write(device.getName() + " vypnuta = OFF");
        device.setState(new OffState());
    }

    @Override
    public void turnOn(Device device) {
        write(device.getName() + " zapnuta = ON");
        device.setState(new OnState());
    }
}
