package andrajak.smarthome.device.state;

import andrajak.smarthome.device.Device;

public interface DeviceState {

    void turnOff(Device device);
    void turnOn(Device device);
    void turnIdle(Device device);
}
