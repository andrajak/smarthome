package andrajak.smarthome.device.state;

import andrajak.smarthome.device.Device;
import andrajak.smarthome.report.WritableActivityEventReport;

public class OnState implements DeviceState, WritableActivityEventReport {

    @Override
    public void turnIdle(Device device) {
        write(device.getName() + " nečinná = IDLE");
        device.setState(new IdleState());
    }

    @Override
    public void turnOff(Device device) {
        write(device.getName() + " vypnuta = OFF");
        device.setState(new OffState());
    }

    @Override
    public void turnOn(Device device) {
    }
}
