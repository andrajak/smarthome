package andrajak.smarthome.device.state;

import andrajak.smarthome.device.Device;
import andrajak.smarthome.report.WritableActivityEventReport;

public class OffState implements DeviceState, WritableActivityEventReport {

    @Override
    public void turnIdle(Device device) {
        write(device.getName() + " nečinná = IDLE");
        device.setState(new IdleState());
    }

    @Override
    public void turnOff(Device device) {
    }

    @Override
    public void turnOn(Device device) {
        write(device.getName() + " zapnuta = ON");
        device.setState(new OnState());
    }

}
