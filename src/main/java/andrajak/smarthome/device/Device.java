package andrajak.smarthome.device;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import andrajak.smarthome.device.state.DeviceState;
import andrajak.smarthome.event.Break;
import andrajak.smarthome.event.Event;
import andrajak.smarthome.observer.Observer;
import andrajak.smarthome.observer.Subject;
import andrajak.smarthome.device.state.OffState;
import andrajak.smarthome.report.WritableActivityEventReport;

import static andrajak.smarthome.device.Constants.*;

public abstract class Device implements Subject, WritableActivityEventReport {

    private String name;
    private int lifetime;
    private boolean isAlive;
    private DeviceState state;
    private final List<Observer> observers = new ArrayList<>();

    private double totalGas = 0;
    private double totalWater = 0;
    private double totalElectricity = 0;

    public Device(String name) {
        this.name = name;
        this.state = new OffState();
        this.lifetime = LIFETIME;
        this.isAlive = true;
    }

    abstract void calculatedForState(double stateValue);

    private void lifeIsGoingAway() {
        lifetime--;
        if (lifetime == 0) {
            breakDevice();
        }
    }

    public void breakDevice() {
        Event event = new Break(this, Event.Type.BREAK);
        write(this.getName() + " se rozbilo, musíme ho opravit nebo vyhodit.");
        notifyAllObservers(event);
    }

    public void tick() {
        if (!isAlive)
            return;
        lifeIsGoingAway();
        int probability = new Random().nextInt(100);
        if (probability < 10) {
            state.turnIdle(this);
            calculatedForState(IDLE);
        }
        else if (probability < 20) {
            state.turnOn(this);
            calculatedForState(ACTIVE);
        }
        else if (probability < 30) {
            state.turnOff(this);
            calculatedForState(OFF);
        }
    }

    @Override
    public void attach(Observer observer) {
        observers.add(observer);
    }

    @Override
    public void detach(Observer observer) {
        observers.remove(observer);
    }

    @Override
    public void notifyAllObservers(Event event) {
        for(Observer observer : observers){
            observer.update(event);
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DeviceState getState() { return state; }

    public void setState(DeviceState state) { this.state = state; }

    public double getTotalElectricity() {
        return totalElectricity;
    }

    public void setTotalElectricity(double totalElectricity) { this.totalElectricity = totalElectricity; }

    public double getTotalWater() {
        return totalWater;
    }

    public void setTotalWater(double totalWater) {
        this.totalWater = totalWater;
    }

    public double getTotalGas() {
        return totalGas;
    }

    public void setTotalGas(double totalGas) { this.totalGas = totalGas; }

    public int getLifetime() { return lifetime; }

    public void setLifetime(int lifetime) { this.lifetime = lifetime; }

    public boolean isAlive() { return isAlive; }

    public void setAlive(boolean alive) { isAlive = alive; }
}
