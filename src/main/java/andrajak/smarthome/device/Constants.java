package andrajak.smarthome.device;

public final class Constants {

    public static final int LIFETIME = 100;

    public static final double ACTIVE = 1;
    public static final double IDLE = 0.25;
    public static final double OFF = 0;

    public static final double GAS_PRICE = 3.5;
    public static final double WATER_PRICE = 0.7;
    public static final double ELECTRICITY_PRICE = 1.5;

    public static final double AIR_CONDITIONING = 42;
    public static final double FRIDGE = 12;
    public static final double GAS_BOILER = 60;
    public static final double HOOVER = 30;
    public static final double MICROWAVE = 18;
    public static final double TELEVISION = 24;
    public static final double TOASTER = 10;
    public static final double WASHING_MACHINE_WATER = 55;
    public static final double WASHING_MACHINE_ELECTRICITY = 42;

    public static final double FRIDGE_CAPACITY = 10;
}
