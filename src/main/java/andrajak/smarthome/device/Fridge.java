package andrajak.smarthome.device;

import andrajak.smarthome.event.Event;
import andrajak.smarthome.event.FillFridge;
import andrajak.smarthome.report.WritableActivityEventReport;

import static andrajak.smarthome.device.Constants.*;

public class Fridge extends Device implements WritableActivityEventReport {

    private double capacity;

    public Fridge(String name) {
        super(name);
        this.capacity = FRIDGE_CAPACITY;
    }

    private void isAlmostEmpty() {
        Event event = new FillFridge(this, Event.Type.FILL_FRIDGE);
        write(this.getName() + " je téměř prázdná.");
        notifyAllObservers(event);
    }

    public void minusFood() {
        this.capacity--;
        if (this.capacity < 3)
            isAlmostEmpty();
    }

    @Override
    public void calculatedForState(double stateValue) {
        setTotalElectricity(getTotalElectricity() + stateValue * FRIDGE);
    }

    public double getCapacity() { return capacity; }

    public void setCapacity(double capacity) { this.capacity = capacity; }
}
