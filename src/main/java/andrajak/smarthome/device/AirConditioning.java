package andrajak.smarthome.device;

import static andrajak.smarthome.device.Constants.AIR_CONDITIONING;

public class AirConditioning extends Device {

    public AirConditioning(String name) {
        super(name);
    }

    @Override
    public void calculatedForState(double stateValue) {
        setTotalElectricity(getTotalElectricity() + stateValue * AIR_CONDITIONING);
    }

}
