package andrajak.smarthome.device;

import andrajak.smarthome.report.WritableActivityEventReport;

import static andrajak.smarthome.device.Constants.AIR_CONDITIONING;
import static andrajak.smarthome.device.Constants.HOOVER;

public class Hoover extends Device implements WritableActivityEventReport {

    public Hoover(String name) {
        super(name);
    }

    @Override
    public void calculatedForState(double stateValue) {
        setTotalElectricity(getTotalElectricity() + stateValue * HOOVER);
    }

}
