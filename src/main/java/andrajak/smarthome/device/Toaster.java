package andrajak.smarthome.device;

import andrajak.smarthome.report.WritableActivityEventReport;

import static andrajak.smarthome.device.Constants.AIR_CONDITIONING;
import static andrajak.smarthome.device.Constants.TOASTER;

public class Toaster extends Device implements WritableActivityEventReport {

    public Toaster(String name) {
        super(name);
    }

    @Override
    public void calculatedForState(double stateValue) {
        setTotalElectricity(getTotalElectricity() + stateValue * TOASTER);
    }

}
