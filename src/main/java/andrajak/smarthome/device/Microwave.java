package andrajak.smarthome.device;

import andrajak.smarthome.report.WritableActivityEventReport;

import static andrajak.smarthome.device.Constants.AIR_CONDITIONING;
import static andrajak.smarthome.device.Constants.MICROWAVE;

public class Microwave extends Device implements WritableActivityEventReport {

    public Microwave(String name) {
        super(name);
    }

    @Override
    public void calculatedForState(double stateValue) {
        setTotalElectricity(getTotalElectricity() + stateValue * MICROWAVE);
    }

}
