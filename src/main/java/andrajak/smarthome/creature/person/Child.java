package andrajak.smarthome.creature.person;

import static andrajak.smarthome.creature.person.ConstantsEnergy.CHILD;

public class Child extends Person{

    public Child(String name, Sex sex) {
        super(name, sex);
        setType(Type.child);
        setEnergy(CHILD);
    }

    @Override
    public void needSleep() {
        write(this.getName() + " je unavený, jde si odpočinout.");
        setEnergy(CHILD);
    }

    @Override
    protected void tickPersonLogic() {
        changeRoom();
    }

}
