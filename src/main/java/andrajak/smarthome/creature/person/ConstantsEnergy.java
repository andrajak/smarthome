package andrajak.smarthome.creature.person;

public final class ConstantsEnergy {

    public static final int ADULT = 8;
    public static final int CHILD = 6;
    public static final int BABY = 3;
}
