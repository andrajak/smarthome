package andrajak.smarthome.creature.person;

import andrajak.smarthome.event.ChangeRoom;
import andrajak.smarthome.event.Event;
import andrajak.smarthome.event.TakeFood;
import andrajak.smarthome.observer.Observer;
import andrajak.smarthome.observer.Subject;
import andrajak.smarthome.place.Room;
import andrajak.smarthome.report.WritableActivityEventReport;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public abstract class Person implements Subject, WritableActivityEventReport {

    private String name;
    private Sex sex;
    private int energy;
    private Type type;
    private Room room;
    private final List<Observer> observers = new ArrayList<>();

    public enum Sex {
        male, female
    }

    public enum Type {
        adult, child, baby
    }

    public Person(String name, Sex sex) {
        this.name = name;
        this.sex = sex;
    }

    public abstract void needSleep();

    public void isHungry() {
        Event event = new TakeFood(this, Event.Type.TAKE_FOOD);
        notifyAllObservers(event);
    }

    public void changeRoom() {
        Event event = new ChangeRoom(this, Event.Type.CHANGE_ROOM);
        notifyAllObservers(event);
    }

    public void tick() {
        this.energy--;
        if (energy == 0)
            needSleep();
        else {
            int probability = new Random().nextInt(100);
            if (probability < 20)
                isHungry();
            else if (probability < 40)
                changeRoom();
            else if (probability < 60)
                tickPersonLogic();
        }
    }

    protected abstract void tickPersonLogic();

    @Override
    public void attach(Observer observer) {
        observers.add(observer);
    }

    @Override
    public void detach(Observer observer) {
        observers.remove(observer);
    }

    @Override
    public void notifyAllObservers(Event event) {
        for(Observer observer : observers){
            observer.update(event);
        }
    }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public Sex getSex() { return sex; }

    public void setSex(Sex sex) { this.sex = sex; }

    public int getEnergy() { return energy; }

    public void setEnergy(int energy) { this.energy = energy;}

    public Type getType() { return type; }

    public void setType(Type type) { this.type = type; }

    public Room getRoom() { return room; }

    public void setRoom(Room room) { this.room = room; }
}
