package andrajak.smarthome.creature.person;

import static andrajak.smarthome.creature.person.ConstantsEnergy.ADULT;

public class Adult extends Person{

    public Adult(String name, Sex sex) {
        super(name, sex);
        setType(Type.adult);
        setEnergy(ADULT);
    }

    @Override
    public void needSleep() {
        write(this.getName() + " je unavený, jde si odpočinout.");
        setEnergy(ADULT);
    }

    @Override
    protected void tickPersonLogic() {
        changeRoom();
    }
}
