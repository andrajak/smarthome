package andrajak.smarthome.creature.person;

import andrajak.smarthome.event.Event;
import andrajak.smarthome.event.Repack;

import static andrajak.smarthome.creature.person.ConstantsEnergy.BABY;

public class Baby extends Person {

    public Baby(String name, Sex sex) {
        super(name, sex);
        setType(Type.baby);
        setEnergy(BABY);
    }

    @Override
    public void needSleep() {
        write(this.getName() + " je unavený, jde si odpočinout.");
        setEnergy(BABY);
    }

    private void needRepack() {
        Event event = new Repack(this, Event.Type.REPACK);
        write(this.getName() + " začíná smrdět, potřebuje přebalit.");
        notifyAllObservers(event);
    }

    @Override
    protected void tickPersonLogic() {
        needRepack();
    }

}
