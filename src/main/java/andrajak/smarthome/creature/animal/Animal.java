package andrajak.smarthome.creature.animal;

import andrajak.smarthome.event.Event;
import andrajak.smarthome.event.Hungry;
import andrajak.smarthome.event.Pee;
import andrajak.smarthome.observer.Observer;
import andrajak.smarthome.observer.Subject;
import andrajak.smarthome.report.WritableActivityEventReport;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public abstract class Animal implements Subject, WritableActivityEventReport {

    private String name;
    private String type;
    private final List<Observer> observers = new ArrayList<>();

    public Animal (String name) {
        this.name = name;
    }

    public void isHungry(){
        Event event = new Hungry(this, Event.Type.HUNGRY);
        write(this.getName() + " má hlad a začíná být otravný.");
        notifyAllObservers(event);
    }

    public void needPee(){
        Event event = new Pee(this, Event.Type.PEE);
        write(this.getName() + " potřebuje vyvenčit, otravnost prudce stoupá.");
        notifyAllObservers(event);
    }

    public void tick() {
        int probability = new Random().nextInt(100);
        if (probability < 25) {
            isHungry();
        }
        else if (probability < 50) {
            needPee();
        }
    }

    @Override
    public void attach(Observer observer) {
        observers.add(observer);
    }

    @Override
    public void detach(Observer observer) {
        observers.remove(observer);
    }

    @Override
    public void notifyAllObservers(Event event) {
        for(Observer observer : observers){
            observer.update(event);
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() { return type; }

    public void setType(String type) { this.type = type; }
}
