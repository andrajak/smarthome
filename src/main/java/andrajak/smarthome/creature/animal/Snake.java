package andrajak.smarthome.creature.animal;

public class Snake extends Animal {

    public Snake(String name) {
        super(name);
        setType("had");
    }
}