package andrajak.smarthome.event;

import andrajak.smarthome.creature.person.Person;

public class ChangeRoom extends Event {

    private Person person;

    public ChangeRoom(Person person, Type type) {
        super(type);
        this.person = person;
        this.setNeedSolver(false);
    }

    public Person getPerson() { return person; }

    public void setPerson(Person person) { this.person = person; }
}
