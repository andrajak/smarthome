package andrajak.smarthome.event;

public abstract class Event {

    private Type type;
    private boolean needSolver;

    public enum Type {
        PEE, REPACK, BREAK, HUNGRY, TAKE_FOOD, FILL_FRIDGE, CHANGE_ROOM
    }

    public Event(Type type) {
        this.type = type;
    }

    public Type getType() { return type; }

    public void setType(Type type) { this.type = type; }

    public boolean isNeedSolver() { return needSolver; }

    public void setNeedSolver(boolean needSolver) { this.needSolver = needSolver; }
}
