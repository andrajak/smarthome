package andrajak.smarthome.event;

import andrajak.smarthome.device.Device;

public class FillFridge extends Event {

    private Device device;

    public FillFridge(Device device, Type type) {
        super(type);
        this.device = device;
        this.setNeedSolver(true);
    }

    public Device getDevice() { return device; }

    public void setDevice(Device device) { this.device = device; }
}