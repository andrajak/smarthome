package andrajak.smarthome.event;

import andrajak.smarthome.creature.animal.Animal;

public class Hungry extends Event {

    private Animal animal;

    public Hungry(Animal animal, Type type) {
        super(type);
        this.animal = animal;
        this.setNeedSolver(true);
    }

    public Animal getAnimal() { return animal; }

    public void setAnimal(Animal animal) { this.animal = animal; }
}