package andrajak.smarthome.event;

import andrajak.smarthome.creature.animal.Animal;

public class Pee extends Event {

    private Animal animal;

    public Pee(Animal animal, Type type) {
        super(type);
        this.animal = animal;
        this.setNeedSolver(true);
    }

    public Animal getAnimal() { return animal; }

    public void setAnimal(Animal animal) { this.animal = animal; }
}