package andrajak.smarthome.event;

import andrajak.smarthome.creature.person.Person;

public class Repack extends Event {

    private Person person;

    public Repack(Person person, Type type) {
        super(type);
        this.person = person;
        this.setNeedSolver(true);
    }

    public Person getPerson() { return person; }

    public void setPerson(Person person) { this.person = person; }
}